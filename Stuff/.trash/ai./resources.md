Contains resources for all things AI


## Tutorials
 - [Karparthy](https://karpathy.ai/zero-to-hero.html)
 - 

## Books

## Papers

## Informative videos

## Blogs
- [distill](https://distill.pub/)
- [Machine learning is fun](https://www.machinelearningisfun.com/)
## Meta stuff
- [pathmind wiki](https://wiki.pathmind.com/)
- 
