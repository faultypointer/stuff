---
tags:
  - ai
---
Date: 2023-01-11

## What is this folder?
It serves as a outlet, reference, and everything related to AI. For a long time I have been interested in Artificial intelligence but haven't been able to do any concrete projects or learn something completely related to it. So this is my attempt to make myself not lose focus of what I really want to do.

## What are my plans for it?
Everything AI related, whether it is a new topic, a project, learning something, I'm going to document it here. I will also set weekly goals for readind books, completing tutorial, etc for AI. 
