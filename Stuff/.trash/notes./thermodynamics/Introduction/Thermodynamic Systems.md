
## System, Boundary and Surroundings

### System
A system is a finite quantity of matter or a prescribed region of space.

### Boundary
The actual or imaginary seperation which encloses the system is called it's boundary.

### Surrounding
Everything outside the system is called surrounding.

## Types of Systems
