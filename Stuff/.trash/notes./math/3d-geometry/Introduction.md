### Distance between two points
P = (x1, y1, z1)
Q = (x2, y2, z2)

PQ = $$\sqrt((x2-x1)^2 + (y2-y1)^2 + (z2-z1)^2)$$

## Ratio
If R(x, y, z) divides P(x1, y1, z1) and Q(x2, y2, z2) internally in the ratio of m1:m2 then
x = $$\frac{m1x2 + m2x1}{m1 + m2}$$
y = $$\frac{m1y2 + m2y1}{m1 + m2}$$
z = $$\frac{m1z2 + m2z1}{m1 + m2}$$

## Projection

### Projection of a point on a line
projection of a point on a given line is the foot of perpendicular drawn from the point to 
the line

### Projection of a line segment on a given line
If AB be a line which make an angle of $\theta$ with the line OX then the projection of AB on the line OX is given by 
A'B' = ABcos($\theta$)
where A' and B' are the foot of perpendicular drawn from A and B to the line OX.


## Direction Cosines
Direction Cosines of any straight line in space are the cosine angles made by the line with the positive direction of the axes. If $\alpha$, $\beta$, $\gamma$ be the angles made by the line with positive direction of axes, then cos($\alpha$), cos($\beta$), cos($\gamma$), are the direction cosines of the line. 

Generally, direction cosines are denoted by l, m, n. Thus,
	l = cos($\alpha$), m = cos($\beta$), n = cos($\gamma$)


### A relation in l, m, n of a line
If l, m, n be the direction cosines of a line, then $l^2 + m^2 + n^2 = 1$.

### Direction Cosines of a Line
*To find the direction cosines of then lines whose direction ratios are given*
If a, b, c, be the direction ratios of a line then,


$$
\begin{aligned}
l = {\pm} {\frac{a}{\sqrt{a^2+b^2+c@}}}, {\;}{\;} 
m = {\pm} {\frac{b}{\sqrt{a^2+b^2+c@}}}, {\;}{\;}
n = {\pm} {\frac{c}{\sqrt{a^2+b^2+c2}}}
\end{aligned}
$$

### Direction Cosines and Ratios of the line joining two points
$$
\begin{aligned}
l = \frac{x2-x1}{r}, {\;}{\;}{\;}
m = \frac{y2-y1}{r}, {\;}{\;}{\;}
n = \frac{z2-z1}{r}
\end{aligned}
$$
where r = PQ
Thus the direction ratios of the line joining the points P (x1, y1, z1) and Q(x2, y2, z2) are:
x2-x1, y2-y1, z2-z1

## Angle Between Two lines
Angle between two lines whose direction cosines are $l_1, m_1, n_1$ and $l_2, m_2, n_2$  is given by
$$\cos\theta = l_1l_2 + m_1m_2 +n_1n_2$$
#### Conditions
Perpendicular:
$$l_1l_2 + m_1m_2 +n_1n_2 = 0$$
Parallel:
$$
\frac{l_1}{l_2} = \frac{m_1}{m_2} = \frac{n_1}{n_2}
$$
