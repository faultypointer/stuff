A plane is a surface such that if any two points be taken on it, then the line joining them lies wholly in the surface.

## General Equation of a Plane
	ax + by + cz + d = 0

## Equation of the Plane

### One Point Form
Equation of a plane passing through a point $(x_1, y_1, z_1)$ is:
$$a(x-x_1) + b(y-y_1) + c(z-z_1) = 0$$
#### Cor.
The equation of a plane passing through the origin $(0, 0, 0)$ is:
$$ax + by + cz = 0$$

### Three Points Form
The Equation of a plane passing though points $(x_1, y_1, z_1), (x_2, y_2, z_2) and (x_3, y_3, z_3)$ is:
$$
\begin{vmatrix}
x-x_1 & y-y_1 & z-z_1 \\
x_2-x_1 & y_2-y_1 & z_2-z_1 \\
x_3-x_1 & y_3-y_2 & z_3-z_1 \\
\end{vmatrix} {\;}
 = {\;} 0
$$
#### Cor.
The four points $(x_1, y_1, z_1), (x_2, y_2, z_2), (x_3, y_3, z_3) and (x_4, y_4, z_4)$ lie on a plane if and only if
$$
\begin{vmatrix}
x_1 & y_1 & z_1 & 1 \\
x_2 & y_2 & z_2 & 1 \\
x_3 & y_3 & z_3 & 1 \\
x_4 & y_4 & z_4 & 1 \\
\end{vmatrix} {\;}
 = {\;} 0
$$
### Intercept Form
Equation of a plane in intercept form
$$
\frac{x}{a} + \frac{y}{b} + \frac{z}{c} = 1
$$
### Normal Form
Equation of a plane in normal form
$$lx + my + nz = p$$
where p is the length of perpendicular from origin to the plane.

## Angle Between Two Planes
$$
\cos\theta = \frac{a_1a_2 + b_1b_2 + c_1c_2}{\sqrt{a_1^2 + b_1^2 + c_1^2}\sqrt{a_2^2 + b_2^2 + c_2^2}}
$$
#### Perpendicular
$$a_1a_2 + b_1b_2 + c_1c_2 = 0$$
#### Parallel
$$
\frac{a_1}{a_2} = \frac{b_1}{b_2} = \frac{c_1}{c_2}
$$
