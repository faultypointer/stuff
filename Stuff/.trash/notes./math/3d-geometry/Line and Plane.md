## Straight Line
A straight Line can be thought as the locus of the common points of two intersecting planes.

### Equation of a Line in General Form:
$$a_1x + b_1y + c_1z + d_1 = 0 = a_2x + b_2y + c_2z + d_2$$

### Equation of Line in Symmetrical Form
*Equation of the line passing through the point $A(x_1, y_1, z_1)$ with direction cosines $l, m, n$ is*
$$
\frac{x-x_1}{l} = \frac{y-y_1}{m} = \frac{z-z_1}{n} (= r)
$$
**Note: Any point of the straight line is $(lr+x_1, mr+y_1, nr+z_1)$**

*Equation of the line passing through the points $A(x_1, y_1, z_1)$ and $A(x_2, y_2, z_2)$ is*
$$
\frac{x-x_1}{x_2-x_1} = \frac{y-y_1}{y_2-y_1} = \frac{z-z_1}{z_2-z_1}
$$

## Angle Between a Line and a Plane
*Angle between the line $\frac{x-x_1}{l} = \frac{y-y_1}{m} = \frac{z-z_1}{n}$ and the plane $ax+by+cz+d = 0$ is*
$$
\sin\theta = \frac{al+bm+cn}{\sqrt{l^2+m^2+n^2}\sqrt{a^2+b^2+c^2}}
$$
### Cor
*If the line is parallel to the plane (perpendicular to the normal to the plane) then*
$$al + bm + cn = 0$$
*If the line is perpendicular to the plane (parallel to normal to the plane) then*
$$
\frac{l}{a} = \frac{m}{b} = \frac{n}{c}
$$
