## Hello World


### Importing pyglet
```python
import pyglet
```

### Creating Window
```python
window = pyglet.window.Window()
```

More about [[Windowing | window]]

### Displaying text
label is used to display the text.
```python
label = pyglet.text.Label('Hello World',
						  font_name='Times New Roman',
						  font_size=36,
						  x = window.width // 2, y = window.width // 2,
						  anchor_x = 'center', anchor_y = 'center')
```

More on [[Displaying Text | displaying text]]

### Handling Events
window dispatches an on_draw event whenever it is ready to redraw its content. pyglet provides several ways to handle such events. A simple way to do it is using [[Decorators | decorators]].

```python
@window.event
def on_draw():
	window.clear()
	label.draw()
```


### Running the app
```python
pyglet.app.run()
```


## Image Viewer

```python
import pyglet

window = pyglet.window.Window()
image = pyglet.resource.image('~/dotfiles/wallpaper/.wallpaper/wallhaven-2kv2gg_1920x1080.png')

@window.event
def on_draw():
	window.clear()
	image.blit(0, 0)

pyglet.app.run()
```

More about [[Images and Sprites | images]]


## Handling Mouse and Keyboard events

### Keyboard events
keyboard events have two parameters; virtual key *symbol* that was pressed and the bitwise combination of any *modifier*s that are present.

```python
import pyglet

window = pyglet.window.Window()

@window.event
def on_key_press(symbol, modifier):
	print(f"The key {symbol} was pressed with modifiers {modifier}.")


@window.event
def on_draw():
	window.clear()


pyglet.app.run()
```


### Mouse events
Mouse events have 4 parameters; position of mouse when it was pressed given by *x* and *x*, the button info given by *button* and *modifier*.

```python

@window.event
def on_mouse_press(x, y, button, modifier):
	print(f"position \{x: {x}, y: {y}\})
	print(f"button: {button}")
	print(f"modifier: {modifier}")
```

