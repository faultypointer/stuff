```c++
extern "C" void _start() {
	// entry point
}
```

## Compiling
```bash
g++ -ffreestanding -nostdlib -mno-red-zone -o binary main.cpp
```

- ffreestanding: specifies that the program doesn't need standrad library
- nostdlib: tells compiler not to include standrad library
- mno-red-zone: disable red zone (optimization feature used by stdlib)1