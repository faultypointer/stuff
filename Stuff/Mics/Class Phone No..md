| RollNo | Name | Phone No |
| --- | --- | --- |
| PUR078BCT049 | MD Astafar Alam   |  9824252229 |
| PUR078BCT050 | Milan Arjel   |  9861445994 |
| PUR078BCT051 | Milan Pokhrel   |  9817340495 |
| PUR078BCT052 | Mithun Yadav    |  9815041051 |
| PUR078BCT053 | Nigam Yadav    |  9806285040 |
| PUR078BCT054 | Nishan Bhattarai    |  9807311140 |
| PUR078BCT055 | Pooja Rana    |  9828717343 |
| PUR078BCT056 | Prasanga Niroula | 9862364021 |
| PUR078BCT057 | Priyanka Shandilya    |  9828717343 |
| PUR078BCT058 | Rahul Rouniyar    |  9807386833 |
| PUR078BCT059 | Rajat Pradhan    |  9811021942 |
| PUR078BCT060 | Rajat Chandra Jha | 980-3470982 |
| PUR078BCT061 | Rajeesh Paandey    |  9841208969 |
| PUR078BCT062 | Ramesh (Ram Chandra) Ghimire    |  9804895941 |
| PUR078BCT063 | Ranjit Adhikari | 9862673820 |
| PUR078BCT064 | Ratan Thapa    |  9841271301 |
| PUR078BCT065 | Ravi Pandit | 9825335399 |
| PUR078BCT066 | Reshmi Jha    |  9881341069 |
| PUR078BCT067 | Rijan Karki    |  9804350249 |
| PUR078BCT068 | Ritesh Sahani    |  9811208126 |
| PUR078BCT069 | Ritika Niraula    |  9862182800 |
| PUR078BCT070 | Rohan Shah    |  9815397788 |
| PUR078BCT071 | Roshan Karki   |  9827489335 |
| PUR078BCT072 | Sagar Katuwal    |  9804057227 |
| PUR078BCT073 | Samir Bidari    |  9811261550 |
| PUR078BCT074 | Sanam Bastola    |  9815316398 |
| PUR078BCT075 | Sandeep Poudel  | 9749240180 |
| PUR078BCT076 | Sandesh Pokharel    |  9862354757 |
| PUR078BCT077 | Sandhya Shrestha  | 9741667020 |
| PUR078BCT078 | Sangyog Puri    |  9824873455 |
| PUR078BCT079 | Sanskar Rijal    |  9844451341 |
| PUR078BCT080 | Saurav Khanal   |  9820355089 |
| PUR078BCT081 | Shubkant Chaudhari    |  9807726772 |
| PUR078BCT082 | Shyam K. Gupta    |  9816751098 |
| PUR078BCT083 | Sneha Patel    |  9829278236 |
| PUR078BCT084 | Sonu Gupta    |  9819094640 |
| PUR078BCT085 | Sony Chaudhary    |  9827658721 |
| PUR078BCT086 | Spandan Guragain   | 9848278935 |
| PUR078BCT087 | Subash Kumar Yadav   | 9811202751 |
| PUR078BCT088 | Sudesh Subedi   | 984 0114485 |
| PUR078BCT089 |     |    |
| PUR078BCT090 | Sujan Nainawasti  | 9749711064 |
| PUR078BCT091 | Sujit Kumar Das   | 9827737859 |
| PUR078BCT092 | Suvash Giri   | 9841889089 |
| PUR078BCT093 | Tayama Bantawa   | 9808501847 |
| PUR078BCT094 | Vision Peer   | 9840189932 |
| PUR078BCT095 | Yam Guragain   | 9811092556 |
| PUR078BCT096 | Yamraj Khadka   | 9808487862 |
