---
cssclass: dashboard
banner: "![[home.jpg]]"
banner_x: 0.5
banner_y: 0.5
---

# Blogs
- Rant
	- [[You can never forget your first.]]
- A bite of
	- [[blogs/a_bite_of/Introduction]]
	- [[Markov Chain]]
# Projects



# Vault Info
- 🗄️ Recent file updates
 `$=dv.list(dv.pages('').sort(f=>f.file.mtime.ts,"desc").limit(4).file.link)`
%%- 🔖 Tagged:  favorite 
 `$=dv.list(dv.pages('#favorite').sort(f=>f.file.name,"desc").limit(4).file.link)`%%
- 〽️ Stats
	-  File Count: `$=dv.pages().length`