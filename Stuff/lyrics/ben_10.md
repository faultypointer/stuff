
It started when an alien device did what it did  
And stuck itself upon his wrist with secrets that it hid
Now he's got superpowers, he's no ordinary kid
He's Ben 10 (Ben 10)

So if you see him
you might be in
for a big surprise
He will turn into an alien
before you very eyes
He is slimy, creepy, fast and strong
he is every shape and size
He is BEN 10

Armed with power he is on the case
fighting off evil from earth or space
He'll never stop 'till he makes them pay
cause he is the baddest kid ever to save the day
He is BEN 10