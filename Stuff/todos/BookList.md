---

kanban-plugin: basic

---

## Want to read

- [ ] Introduction to algorithms
- [ ] The emotion machine
- [ ] Clean code
- [ ] To kill a mockingbird
- [ ] Design Patterns
- [ ] The Great Gatsby


## Reading

- [ ] Crime and Punishment


## Pause

- [ ] Programming Rust


## Completed

- [ ] Norwegian wood
- [ ] Kafka on the shore
- [ ] The silent patient
- [ ] Neural network from scratch


## Artificial Intellicence books

- [ ] Artificial intelligence with python
- [ ] machine learning yearning
- [ ] deep learning with python
- [ ] reinforcement learning - richard sutton
- [ ] ai modern approach




%% kanban:settings
```
{"kanban-plugin":"basic"}
```
%%